from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    technician_name = models.CharField(max_length=50)
    employee_number = models.IntegerField(unique=True)


    def __str__(self):
        return self.technician_name

    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"pk": self.pk})


class Appointment(models.Model):
    customer_name = models.CharField(max_length=50)
    appointment_time = models.DateTimeField(null=True)
    appointment_details = models.CharField(max_length=200)
    technician = models.CharField(max_length=50)
    is_completed = models.BooleanField(default=False)
    vip = models.BooleanField(default=False)
    vin = models.CharField(max_length=17)

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.pk})
