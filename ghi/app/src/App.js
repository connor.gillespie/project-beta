import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import SaleRecordList from './Sales/SaleRecordList';
import AddSalesPerson from './Sales/SalesPersonForm';
import AddSaleRecord from './Sales/SaleRecordForm';
import AddCustomer from './Sales/CustomerForm';
import SalesPersonHistory from './Sales/SalesPersonHistory';
import AppointmentList from './Services/AppointmentList';
import AddTechnician from './Services/TechnicianForm';
import AddAppointment from './Services/AppointmentForm';
import ServiceHistory from './Services/ServiceHistory';
import ManufacturerList from './Inventory/ManufacturerList';
import CreateManufacturer from './Inventory/ManufacturerForm';
import ModelsList from './Inventory/ModelsList';
import CreateModel from './Inventory/ModelForm';
import AutomobilesList from './Inventory/AutomobileList';
import CreateAutomobile from './Inventory/AutomobileForm';
import Nav from './Nav';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers/">
            <Route index element={<ManufacturerList />} />
            <Route path="new" element={<CreateManufacturer />} />
          </Route>
          <Route path="models/">
            <Route index element={<ModelsList />} />
            <Route path="new" element={<CreateModel />} />
          </Route>
          <Route path="automobiles/">
            <Route index element={<AutomobilesList />} />
            <Route path="new" element={<CreateAutomobile />} />
          </Route>
          <Route path="salerecords/">
            <Route index element={<SaleRecordList />} />
            <Route path="new" element={<AddSaleRecord />} />
          </Route>
          <Route path="salessearch" element={<SalesPersonHistory />} />
          <Route path="salesperson/">
              <Route path="new" element={<AddSalesPerson />} />
          </Route>
          <Route path="customers/">
              <Route path="new" element={<AddCustomer />} />
          </Route>
          <Route path="technician/">
            <Route path="new" element={<AddTechnician />} />
          </Route>
          <Route path="appointment/">
            <Route index element={<AppointmentList />} />
            <Route path="new" element={<AddAppointment />} />
            <Route path="history" element={<ServiceHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
