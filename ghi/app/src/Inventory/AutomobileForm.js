import React from "react";

class CreateAutomobile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vin: "",
            color: "",
            year: "",
            models: [],
            model: "",
        }
        this.handleChangeVin = this.handleChangeVin.bind(this);
        this.handleChangeColor = this.handleChangeColor.bind(this);
        this.handleChangeYear = this.handleChangeYear.bind(this);
        this.handleChangeModel = this.handleChangeModel.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        const modelUrl = 'http://localhost:8100/api/models/';
        const modelResponse = await fetch(modelUrl);

        if (modelResponse.ok) {
            const modelData = await modelResponse.json();
            this.setState({
                models: modelData.models,
            });
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.model_id = data.model;
        delete data.models;
        delete data.model;

        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const automobileResponse = await fetch(automobileUrl, fetchOptions);
        if (automobileResponse.ok) {
            const newAutomobile = await automobileResponse.json();
            console.log(newAutomobile);
            this.setState({
                vin: "",
                color: "",
                year: "",
                models: [],
                model: "",
            });
            window.location.replace('http://localhost:3000/automobiles/')
        }
    }

    handleChangeVin(event) {
        const value = event.target.value;
        this.setState({ vin: value });
    }

    handleChangeColor(event) {
        const value = event.target.value;
        this.setState({ color: value });
    }

    handleChangeYear(event) {
        const value = event.target.value;
        this.setState({ year: value });
    }

    handleChangeModel(event) {
        const value = event.target.value;
        this.setState({ model: value });
    }

    render() {
        return (
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <h1>Create an automobile</h1>
                  <form onSubmit={this.handleSubmit} id="create-automobile">
                    <div className="form-floating mb-3">
                      <input onChange={this.handleChangeVin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                      <label htmlFor="vin">VIN</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input onChange={this.handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                      <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input onChange={this.handleChangeYear} placeholder="Year" required type="text" name="year" id="year" className="form-control" />
                      <label htmlFor="year">Year</label>
                    </div>
                    <div className="mb-3">
                      <select onChange={this.handleChangeModel} required name="model" id="model" className="form-select">
                        <option value="">Choose a model</option>
                        {this.state.models.map(model => {
                          return (
                            <option key={model.id} value={model.id}>{model.name}</option>
                          )
                        })}
                      </select>
                    </div>
                    <button className="btn btn-dark">Create</button>
                  </form>
                </div>
              </div>
            </div>
        );
    }
}


export default CreateAutomobile;
