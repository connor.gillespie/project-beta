import React from "react";
import { Link } from 'react-router-dom';

class AutomobilesList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            automobiles: [],
        }
    };

    async componentDidMount() {
        const url = 'http://localhost:8100/api/automobiles/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({
                automobiles: data.autos,
            });
        }
    }

    render() {
        return (
          <>
          <div className="px-4 py-4 my-2">
            <div className="col-lg-6 mx-auto text-center">
              <h1 className="display-5 fw-bold">Automobiles</h1>
            </div>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center my-3">
              <Link to="/automobiles/new" className="btn btn-dark btn-lg px-4 gap-3">Create</Link>
            </div>
            <table className="table">
              <thead className="table-dark">
                <tr>
                  <th>VIN</th>
                  <th>Color</th>
                  <th>Year</th>
                  <th>Manufacturer</th>
                  <th>Model</th>
                </tr>
              </thead>
              <tbody>
                {this.state.automobiles.map(automobile => {
                  return (
                    <tr key={automobile.vin}>
                      <td>{ automobile.vin }</td>
                      <td>{ automobile.color }</td>
                      <td>{ automobile.year }</td>
                      <td>{ automobile.model.manufacturer.name }</td>
                      <td>{ automobile.model.name }</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
          </>
        );
    };
}

export default AutomobilesList;
