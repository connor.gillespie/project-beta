import React from "react";


class CreateManufacturer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
    }
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    const data = {...this.state};

    const manufacturersUrl = 'http://localhost:8100/api/manufacturers/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(manufacturersUrl, fetchConfig);
    if (response.ok) {
      this.setState({
        name: "",
      });
      window.location.replace('http://localhost:3000/manufacturers/')
    }
  }

  handleChange = (e) => {
    const value = e.target.value;
    const name = e.target.name;
    this.setState({[name]: value});
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create Manufacturer</h1>
            <form onSubmit={this.handleSubmit} id="create-customer-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"  value={this.state.name}/>
                <label htmlFor="name">Name</label>
              </div>
              <button className="btn btn-dark">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default CreateManufacturer;
