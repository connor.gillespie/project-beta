import React from 'react';

class CreateModel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      picture: "",
      manufacturer: "",
      manufacturers: [],
    }
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ manufacturers: data.manufacturers});
    }
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    const data = {...this.state};
    data.picture_url = data.picture;
    data.manufacturer_id = data.manufacturer;
    delete data.picture;
    delete data.manufacturers;
    delete data.manufacturer;

    const modelUrl = 'http://localhost:8100/api/models/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const modelResponse = await fetch(modelUrl, fetchOptions);
    if (modelResponse.ok) {
      this.setState({
        name: "",
        picture: "",
        manufacturer: "",
        manufacturers: [],
      });
      window.location.replace('http://localhost:3000/models/')
    }
  }

  handleChange = (e) => {
    const value = e.target.value;
    const name = e.target.name;
    this.setState({[name]: value});
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a vehicle model</h1>
            <form onSubmit={this.handleSubmit} id="create-vehicle-model">
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} placeholder="Picture" required type="text" name="picture" id="picture" className="form-control" />
                <label htmlFor="picture">Picture Url</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleChange} required name="manufacturer" id="manufacturer" className="form-select">
                  <option value="">Choose a manufacturer</option>
                  {this.state.manufacturers.map(manufacturer => {
                    return (
                      <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-dark">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }

}

export default CreateModel;
