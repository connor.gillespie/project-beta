import { NavLink } from 'react-router-dom';


function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CARCAR</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="/" role="button" aria-haspopup="true" aria-expanded="false">INVENTORY</a>
              <div className="dropdown-menu dropdown-menu-dark">
                <NavLink className="dropdown-item" to="/manufacturers">Manufacturers</NavLink>
                <NavLink className="dropdown-item" to="/models">Models</NavLink>
                <NavLink className="dropdown-item" to="/automobiles">Automobiles</NavLink>
              </div>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="/" role="button" aria-haspopup="true" aria-expanded="false">SERVICES</a>
                <div className="dropdown-menu dropdown-menu-dark">
                  <NavLink className="dropdown-item" to="/technician/new">Add Technician</NavLink>
                  <NavLink className="dropdown-item" to="/appointment">Appointments</NavLink>
                  <NavLink className="dropdown-item" to="/appointment/history">Service History</NavLink>
                </div>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="/" role="button" aria-haspopup="true" aria-expanded="false">SALES</a>
                <div className="dropdown-menu dropdown-menu-dark">
                  <NavLink className="dropdown-item" to="/salerecords">Sale Records</NavLink>
                  <NavLink className="dropdown-item" to="/salessearch">Sales Search</NavLink>
                  <NavLink className="dropdown-item" to="/salesperson/new">Add Salesperson</NavLink>
                  <NavLink className="dropdown-item" to="/customers/new">Add Customer</NavLink>
                </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
