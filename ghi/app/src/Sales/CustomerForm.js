import React from "react";


class AddCustomer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      address: "",
      number: "",
      isCreated: false,
    }
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    const data = {...this.state};
    delete data.isCreated;

    const customerUrl = 'http://localhost:8090/api/customers/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(customerUrl, fetchConfig);
    if (response.ok) {
      this.setState({
        name: "",
        address: "",
        number: "",
        isCreated: true,
      });
    }
  }

  handleChange = (e) => {
    const value = e.target.value;
    const name = e.target.name;
    this.setState({[name]: value});
  }

  render() {
    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (this.state.isCreated) {
      messageClasses = 'alert alert-success mb-0';
      formClasses = 'd-none';
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add Customer</h1>
            <form className={formClasses} onSubmit={this.handleSubmit} id="create-customer-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"  value={this.state.name}/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} placeholder="Phone Number" required type="number" name="number" id="number" className="form-control" value={this.state.number}/>
                <label htmlFor="number">Phone Number</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} placeholder="Address" required type="text" name="address" id="address" className="form-control" value={this.state.address}/>
                <label htmlFor="address">Address</label>
              </div>
              <button className="btn btn-dark">Create</button>
            </form>
            <div className={messageClasses} id="success-message">
                  Customer Created
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AddCustomer;
