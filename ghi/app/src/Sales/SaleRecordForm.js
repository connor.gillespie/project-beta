import React from 'react';

class AddSaleRecord extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      price: "",
      customer: "",
      automobile: "",
      automobiles: [],
      customers: [],
      salespeople: [],
    }
  }

  async componentDidMount() {
    const automobilesUrl = 'http://localhost:8100/api/automobiles/';
    const salespeopleUrl = 'http://localhost:8090/api/salesperson/';
    const customersUrl = 'http://localhost:8090/api/customers/';
    const salesRecordsUrl = 'http://localhost:8090/api/salerecords/';

    const automobileResponse = await fetch(automobilesUrl);
    const salespeopleResponse = await fetch(salespeopleUrl);
    const customersResponse = await fetch(customersUrl);
    const salesRecordsResponse = await fetch(salesRecordsUrl);

    if (automobileResponse.ok && salespeopleResponse.ok && customersResponse.ok & salesRecordsResponse.ok) {
      const automobileData = await automobileResponse.json();
      const salespeopleData = await salespeopleResponse.json();
      const customersData = await customersResponse.json();
      const salesRecordsData = await salesRecordsResponse.json();
      const soldVins = salesRecordsData.salerecord.map((salerecord) => salerecord.automobile.vin);
      const unsoldCars = automobileData.autos.filter(auto => !soldVins.includes(auto.vin)).map(auto => auto);
      this.setState({
        automobiles: unsoldCars,
        salespeople: salespeopleData.salesperson,
        customers: customersData.customer,
      });
    }
  }
  handleSubmit = async (event) => {
    event.preventDefault();
    const data = {...this.state};
    delete data.salespeople;
    delete data.customers;
    delete data.automobiles;

    const salerecordsUrl = 'http://localhost:8090/api/salerecords/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const salerecordResponse = await fetch(salerecordsUrl, fetchOptions);
    if (salerecordResponse.ok) {
      this.setState({
        automobile: "",
        saleperson: "",
        customer: "",
        price: "",
      });
      window.location.replace('http://localhost:3000/salerecords/')
    }
  }

  handleChange = (e) => {
    const value = e.target.value;
    const name = e.target.name;
    this.setState({[name]: value});
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new autosale</h1>
            <form onSubmit={this.handleSubmit} id="create-autosale-form">
              <div className="mb-3">
                <select onChange={this.handleChange} required name="automobile" id="automobile" className="form-select" value={this.state.automobile}>
                  <option value="">Choose a Automobile</option>
                  {this.state.automobiles.map(automobile => {
                    return (
                      <option key={automobile.id} value={automobile.vin}>{automobile.vin}</option>
                    )
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select onChange={this.handleChange} required name="salesperson" id="salesperson" className="form-select" value={this.state.salesperson}>
                  <option value="">Choose a A Salesperson</option>
                  {this.state.salespeople.map(salesperson => {
                    return (
                      <option key={salesperson.id} value={salesperson.id}>{salesperson.name}</option>
                    )
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select onChange={this.handleChange} required name="customer" id="customer" className="form-select" value={this.state.customer}>
                  <option value="">Choose a A Customer</option>
                  {this.state.customers.map(customer => {
                    return (
                      <option key={customer.id} value={customer.id}>{customer.name}</option>
                    )
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} placeholder="Sale Price" required type="number" name="price" id="price" className="form-control" value={this.state.price}/>
                <label htmlFor="price">Sale Price</label>
              </div>
              <button className="btn btn-dark">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AddSaleRecord;
