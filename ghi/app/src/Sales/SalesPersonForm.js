import React from "react";


class AddSalesPerson extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      number: "",
      isCreated: false,
    }
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    const data = {...this.state};
    delete data.isCreated;

    const salespersonUrl = 'http://localhost:8090/api/salesperson/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(salespersonUrl, fetchConfig);
    if (response.ok) {
      this.setState({
        name: '',
        number: '',
        isCreated: true,
      });
    }
  }

  handleChange = (e) => {
    const value = e.target.value;
    const name = e.target.name;
    this.setState({[name]: value});
  }

  render() {
    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (this.state.isCreated) {
      messageClasses = 'alert alert-success mb-0';
      formClasses = 'd-none';
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add Salesperson</h1>
            <form className={formClasses} onSubmit={this.handleSubmit} id="create-salesperson-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"  value={this.state.name}/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} placeholder="Empolyee Number" required type="number" name="number" id="number" className="form-control" value={this.state.number}/>
                <label htmlFor="number">Employee Number</label>
              </div>
              <button className="btn btn-dark">Create</button>
            </form>
            <div className={messageClasses} id="success-message">
                  Salesperson Created
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AddSalesPerson;
