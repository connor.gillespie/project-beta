import React from "react";

class AddTechnician extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            technician_name: "",
            employee_number: "",
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeTechnician_name = this.handleChangeTechnician_name.bind(this);
        this.handleChangeEmployee_number = this.handleChangeEmployee_number.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};

        const technicialUrl = 'http://localhost:8080/api/technician/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(technicialUrl, fetchConfig);
        if (response.ok) {
            const newTechnician = await response.json();
            console.log(newTechnician);
            this.setState({
                technician_name: '',
                employee_number: '',
            })
            window.location.replace('http://localhost:3000/technician/')
        }
    }

    handleChangeTechnician_name(event) {
        const value = event.target.value;
        this.setState({ technician_name: value });
    }

    handleChangeEmployee_number(event) {
        const value = event.target.value;
        this.setState({ employee_number: value });
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a new technician</h1>
                        <form onSubmit={this.handleSubmit} id="create-technician-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeTechnician_name} placeholder="Technician_name" required type="text" name="technician_name" id="technician_name" className="form-control"  value={this.state.technician_name}/>
                                <label htmlFor="technician_name">Technician Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeEmployee_number} placeholder="Employee_number" required type="text" name="employee_number" id="employee_number" className="form-control"  value={this.state.employee_number}/>
                                <label htmlFor="employee_number">Employee Number</label>
                            </div>
                            <button className="btn btn-dark">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}


export default AddTechnician;
