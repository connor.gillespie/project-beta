import React from "react";
import { Link } from 'react-router-dom';
import moment from 'moment';

class AppointmentList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            appointment: [],
        }
        this.handleDelete = this.handleDelete.bind(this);
        this.handleComplete = this.handleComplete.bind(this);
    };

    async componentDidMount() {
        const url = 'http://localhost:8080/api/appointment/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data);
            this.setState({appointment: data.appointment});
        }
    }

    async handleDelete(id) {
        await fetch(`http://localhost:8080/api/appointment/${id}/`, { method: 'DELETE' })
        this.setState({appointment: this.state.appointment.filter(appointment => appointment.id !== id)});
    }

    async handleComplete(id, appointment) {
        await fetch(`http://localhost:8080/api/appointment/${id}/`, {
            method: 'PUT',
            body: JSON.stringify({
                "is_completed": true
            }),
            headers: {
                'Content-Type': 'application/json',
            },
         })
        this.setState({appointment: this.state.appointment.filter(appointment => appointment.is_completed !== true)})
    }

    render () {
        return (
            <>
            <div className="px-4 py-5 my-2 text-center">
                <div className="col-lg-6 mx-auto text-center">
                    <p className="display-5 fw-bold">
                        Appointments List
                    </p>
                    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center my-3">
                        <Link to="/appointment/new" className="btn btn-dark btn-lg px-4 gap-3 my-1">Add an Appointment</Link>
                    </div>
                </div>
                <table className="table">
                    <thead className="table-dark">
                        <tr>
                            <th>VIN</th>
                            <th>Customer Name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>VIP Status</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.appointment.filter((appointment) => appointment.is_completed === false).map(appointment => {
                            return (
                                <tr key={appointment.href}>
                                    <td>{ appointment.vin }</td>
                                    <td>{ appointment.customer_name }</td>
                                    <td>{ moment(appointment.appointment_time).format('MMM Do YYYY') }</td>
                                    <td>{ moment(appointment.appointment_time).format('h:mm a') }</td>
                                    <td>{ appointment.technician }</td>
                                    <td>{ appointment.appointment_details }</td>
                                    <td>{ appointment.vip.toString() }</td>
                                    <td>
                                        <form>
                                            <button onClick={(event) => this.handleDelete(appointment.id, event)} className="btn btn-danger">Cancel</button>
                                        </form>
                                    </td>
                                    <td>
                                        <form>
                                            <button onClick={(event) => this.handleComplete(appointment.id, event)} className="btn btn-success">Finished</button>
                                        </form>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
            </>
        );
    };
}


export default AppointmentList;
