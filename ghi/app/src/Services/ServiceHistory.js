import React from "react";
import moment from "moment";

class ServiceHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            appointment: [],
            vin: "",
        }

        this.handleChangeInput = this.handleChangeInput.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    async componentDidMount() {
        const appointmentUrl = 'http://localhost:8080/api/appointment/';
        const appointmentResponse = await fetch(appointmentUrl);

        if(appointmentResponse.ok) {
            const appointmentData = await appointmentResponse.json();
            this.setState({appointment: appointmentData.appointment})
        };
    }

    handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state}
        const completedAppointmentList = data.appointment.filter((appointment) => appointment.vin === data.vin)
        this.setState({appointment: completedAppointmentList})
    }

    handleChangeInput(event) {
        const value = event.target.value;
        this.setState({ vin: value });
    }

    render() {
        return (
            <>
            <div className="px-4 py-5 my-2 text-center">
                <div className="col-lg-6 mx-auto text-center">
                    <h1 className="display-5 fw-bold">Service History</h1>
                </div>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <input onChange={this.handleChangeInput} placeholder="Input Automobile VIN" className="form-control my" required value={this.state.vin} />
                        <button className="btn btn-dark my-3" type="submit" id="button-addon">Search</button>
                    </div>
                </form>
                <table className="table">
                <thead className="table-dark">
                    <tr>
                        <th>VIN</th>
                        <th>Customer Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.appointment.map(appointment => {
                        return (
                            <tr key={appointment.id}>
                                <td>{ appointment.vin }</td>
                                <td>{ appointment.customer_name }</td>
                                <td>{ moment(appointment.appointment_time).format('MMM Do YYYY') }</td>
                                <td>{ moment(appointment.appointment_time).format('h:mm a') }</td>
                                <td>{ appointment.technician }</td>
                                <td>{ appointment.appointment_details }</td>
                            </tr>
                        );
                    })}
                </tbody>
                </table>
            </div>
            </>
        )
    }
}

export default ServiceHistory;
