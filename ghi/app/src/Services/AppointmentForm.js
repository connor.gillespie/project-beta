import React from "react";
import moment from "moment";

class AddAppointment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vin: "",
            customer_name: "",
            appointment_time: "",
            technician: "",
            appointment_details: "",
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeVin = this.handleChangeVin.bind(this);
        this.handleChangeCustomer_name = this.handleChangeCustomer_name.bind(this);
        this.handleChangeAppointment_time = this.handleChangeAppointment_time.bind(this);
        this.handleChangeTechnician = this.handleChangeTechnician.bind(this);
        this.handleChangeAppointment_details = this.handleChangeAppointment_details.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};

        const appointmentUrl = 'http://localhost:8080/api/appointment/'
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const appointmentResponse = await fetch(appointmentUrl, fetchOptions);
        if (appointmentResponse.ok) {
            const newAppointment = await appointmentResponse.json();
            console.log(newAppointment);
            this.setState({
                vin: "",
                customer_name: "",
                appointment_time: "",
                technician: "",
                appointment_details: "",
            });
            window.location.replace('http://localhost:3000/appointment/')
        }
    }


    handleChangeVin(event) {
        const value = event.target.value;
        this.setState({ vin: value });
    }

    handleChangeCustomer_name(event) {
        const value = event.target.value;
        this.setState({ customer_name: value });
    }

    handleChangeAppointment_time(event) {
        const value = event.target.value;
        this.setState({ appointment_time: moment(value).toISOString() });
    }

    handleChangeTechnician(event) {
        const value = event.target.value;
        this.setState({ technician: value });
    }

    handleChangeAppointment_details(event) {
        const value = event.target.value;
        this.setState({ appointment_details: value });
    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new Appointment</h1>
                        <form onSubmit={this.handleSubmit} id="create-appointment-form">
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangeVin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control"  value={this.state.vin}/>
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangeCustomer_name} placeholder="Customer_name" required type="text" name="customer_name" id="customer_name" className="form-control"  value={this.state.customer_name}/>
                            <label htmlFor="customer_name">Customer Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangeTechnician} placeholder="Technician" required type="text" name="technician" id="technician" className="form-control"  value={this.state.technician}/>
                            <label htmlFor="technician">Technician</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangeAppointment_details} placeholder="Appointment_details" required type="text" name="appointment_details" id="appointment_details" className="form-control"  value={this.state.appointment_details}/>
                            <label htmlFor="appointment_details">Reason</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangeAppointment_time} placeholder="Appointment_time" name="appointment_time" id="appointment_time" className="form-control"  value={this.state.appointment_time}/>
                            <label htmlFor="appointment_time">Appointment Time</label>
                        </div>
                        <button className="btn btn-dark">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}


export default AddAppointment;
