# CarCar

Team:

* Marvin Lee - Sales

* Connor Gillespie - Service

## Design
![ALT](/image/Project-beta-design.png)

## Service microservice

To fulfill the requirements of the project, I began by making 3 models. I created an AutomobileVO model that I used to grab the vin from the Automobile model within Inventory. I then made a Technician and Appointment model, but made sure to not use a ForeignKey since I needed both of these models to be held separately.

Once the models were completed, I then needed to set up the views portion. I started by creating the encoders that I wanted to use. For this, I kept it really simple. I made an AutomobileVODetailEncoder that was simply used to grab the vin and import_href. I then created a TechnicianEncoder, which was only grabbing the same two properties from the model, and an AppointmentEncoder, which grabbed the same properties from the model plus the id. Once these were completed, I integrated them into the views.

For the views portion it got a bit more complicated. Most of what I did was fairly standard, but I realized I needed to find a way to apply the vip status to an appointment without making any changes to the inventory itself. This was done with a try/except statement, where I essentially tried to grab an object with the vin I was using to create the appointment, and if it was found, the vip status was true, otherwise it was false. This was an effective way of applying the status at the beginning without editing anything else. The rest of the views were a standard set up to GET, PUT, POST, and DELETE for the later forms.



## Sales microservice
The Sales microservice handles keeping track of sales with forms to add new customer, new salespeople and salerecoreds.

Created 4 Models - Customer, Salesperson, SaleRecord and AutomobileVO.
  - Customer
    - name
    - address
    - phone number
  - Salesperson
    - name
    - employee number
  - SaleRecord
    - price
    - automobile
    - salesperson
    - customer
  - AutomobileVO
    - vin
    - import_href

In the Salerecord model salesperson and customers are foreign keys with on_delete=PROTECT so that if any of these attributes are deleted the sale record would not get deleted. Automobile has a one to one relationship with salerecord since a car can only be sold to a customer once.

AutomobileVO model polls data for vin from Autobmobile model in Inventory App with poller.

Create and List views were created for Customer, Salesperson and Salerecord models.

Instead of adding a sold attribute to automobile model in the inventory app, I filtered out sold vehicles from the salesrecord api so that when a salerecord form is being created the drop down of vehicles only shows unsold vehicles.
