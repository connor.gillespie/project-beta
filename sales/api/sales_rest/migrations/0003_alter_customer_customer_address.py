# Generated by Django 4.0.3 on 2022-10-24 21:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0002_customer_customer_address'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='customer_address',
            field=models.CharField(max_length=100),
        ),
    ]
