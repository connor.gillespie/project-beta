from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=100)

    def __str__(self):
        return self.vin


class Salesperson(models.Model):
    name = models.CharField(max_length=100)
    number = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.name


class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    number = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class SaleRecord(models.Model):
    price = models.PositiveIntegerField()
    automobile = models.OneToOneField(
      AutomobileVO,
      related_name="salerecords",
      on_delete=models.PROTECT,
    )

    salesperson = models.ForeignKey(
      Salesperson,
      related_name="salerecords",
      on_delete=models.PROTECT,
    )

    customer = models.ForeignKey(
      Customer,
      related_name="salerecords",
      on_delete=models.PROTECT,
    )
