from common.json import ModelEncoder
from .models import AutomobileVO, Salesperson, Customer, SaleRecord

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
      "vin",
      "import_href",
    ]


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
      "id",
      "name",
      "number",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
      "id",
      "name",
      "address",
      "number",
    ]


class SaleRecordEncoder(ModelEncoder):
    model = SaleRecord
    properties = [
      "id",
      "price",
      "automobile",
      "salesperson",
      "customer",
    ]
    encoders = {
      "automobile": AutomobileVOEncoder(),
      "salesperson": SalespersonEncoder(),
      "customer": CustomerEncoder(),
    }
