from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import AutomobileVO, Salesperson, Customer, SaleRecord
from .encoders import SalespersonEncoder, CustomerEncoder, SaleRecordEncoder
from django.http import JsonResponse
from django.db import IntegrityError
import json


@require_http_methods(["GET", "POST"])
def api_salesperson(request):
    if request.method == "GET":
        salesperson = Salesperson.objects.all()
        return JsonResponse(
            {"salesperson": salesperson},
            encoder=SalespersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)

            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except IntegrityError:
            return JsonResponse(
                {"message": "Employee number already exists"},
                status=400,
            )



@require_http_methods(["GET", "POST"])
def api_customer(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)

            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except IntegrityError:
            return JsonResponse(
                {"message": "Phone number already exists"},
                status=400,
            )


@require_http_methods(["GET", "POST"])
def api_sale_record(request):
    if request.method == "GET":
        salerecord = SaleRecord.objects.all()
        return JsonResponse(
            {"salerecord": salerecord},
            encoder=SaleRecordEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"meesage": "Invalid Automobile Vin"},
                status=400,
            )
        try:
            salesperson = Salesperson.objects.get(id=content["salesperson"])
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"meesage": "Invalid Salesperson"},
                status=400,
            )
        try:
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"meesage": "Invalid Customer"},
                status=400,
            )

        salerecord = SaleRecord.objects.create(**content)
        return JsonResponse(
            salerecord,
            encoder=SaleRecordEncoder,
            safe=False,
        )
